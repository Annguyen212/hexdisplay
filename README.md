# Hex Display
1. Document:
   * [Intel HEX File Format](https://www.keil.com/support/docs/1584/)
   * [Intel HEX wiki](https://en.wikipedia.org/wiki/Intel_HEX)
   * [Error check in y or no answer](https://stackoverflow.com/questions/51579748/error-check-in-y-or-no-answer-c)


2. Follow program:
   * Check Command line: "HexDisplay hexFineName.hex"
   * Read file hex by intel hex format
   * Check file hex syntax
   * Print memory of file hex


3. Clear screen:
   * With linux use: system("clear") in stdlib.h
   * with window use: clrscr() in conio.h
